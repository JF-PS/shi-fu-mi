import AbstractService from "./AbstractService";
import Axios from 'axios';

class ChalengerService extends AbstractService {

    async getChalenger(idChalenger) {
        let promise;
        let user;
        //let url_test = "http://localhost:4000/api/users/"
        let url_server = "http://alexperrot.fr/api/users/"

        await Axios.get(url_server + idChalenger).then(
            (response) => {
                console.log(response);
                user = response.data;
            }
        );

        promise = this.getRequest(user, true);
        return promise;
    }
}

const chalengerService = new ChalengerService();
export default chalengerService;