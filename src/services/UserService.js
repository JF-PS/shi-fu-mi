import AbstractService from "./AbstractService";
import Axios from 'axios';

class UserService extends AbstractService {

    async getUser(idUser) {
        let promise;
        let user;
        //let url_test = "http://localhost:4000/api/users/"
        let url_server = "http://alexperrot.fr/api/users/"

        await Axios.get(url_server + idUser).then(
            (response) => {
                console.log(response);
                user = response.data;
            }
        );

        promise = this.getRequest(user, true);
        return promise;
    }

    async login(login, pass) {

        let promise;
        let user;
        // let url_test = "http://localhost:4000/api/users/"
        let url_server = "http://alexperrot.fr/api/users/"

        await Axios.get(url_server + login + "/" + pass).then(
            (response) => {
                console.log(response);
                user = response.data;
            }
        );

        if(user != null) {
            if(user.password === pass) { promise = this.getRequest(user, true); }
            else { promise = this.getRequest("Wrong password for user " + user.login, false); }
        }
        else {
            promise = this.getRequest("No user exists with login " + login, false);
        }
        return promise;
    }

    async createUser(login, password) {
        let promise;
        let user;
        await Axios.post("http://alexperrot.fr/api/users", {
            login: login, 
            password: password
        }).then(
            (response) => {
                console.log(response);
                user = response.data;
            }
        );

        console.log(user);
        console.log(user.login);
        console.log(user.password);

        if(user != null) {
            if(user.password === password) {
                promise = this.getRequest(user, true);
            }
            else {
                promise = this.getRequest("Wrong password for user " + user.login, false);
            }
        }
        else {
            promise = this.getRequest("No user exists with login " + login, false);
        }
        return promise;
    }

    editUser(id, login, password) {
        //Not enough time to do it correctly
        let user = null;
        for(let i = 0; i<this.users.length; i++) {
            if(this.users[i].id === id) {
                this.users[i].login = login;
                this.users[i].password = password;
                console.log(this.users);
                user = this.users[i];
                break;
            }
        }
        return this.getRequest(user, true);
    }
}

const userService = new UserService();
export default userService;