import React, {useContext, useState} from 'react';
import {useHistory} from 'react-router-dom';
import UserContext from "../../contexts/UserContext.js";
import Loading from "../loading/Loading";
import './Connexion.css';

function Connexion() {
  const [login, setLogin] = useState("test");
  const [password, setPassword] = useState("test");
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();
  const userContext = useContext(UserContext);
    
  return (
<section id="cover" >
    <div id="cover-caption">
        <div class="container">
        <div class="row text-white">
                <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10 mx-auto text-center form p-4">
                    <h1 class="display-4 py-2 text-truncate">Se connecter</h1>
                    <div class="px-2" className="login">
                        <form onSubmit={(e) => {
                            e.preventDefault();
                            setIsLoading(true);
                            userContext.logUser(login, password, (u) => {alert("Bienvenue " + u.login); history.push("/");}, alert, () => setIsLoading(false));
                        }} class="justify-content-center">
                            <div class="form-group">
                                <label class="sr-only" for="login">Login</label>
                                <input name="login" placeholder="login" class="form-control" type="text" value={login} onChange={(e) => setLogin(e.target.value)}/>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="password">Password</label>
                                <input name="password" placeholder="pass" class="form-control" type="password" value={password} onChange={(e) => setPassword(e.target.value)}/>                            </div>
                                { isLoading ? <Loading/> : <button class="btn btn-primary btn-lg" type="submit">Login</button> }
                        </form>
                    </div>
                </div>
            </div>
            </div>
    </div>
</section>

  );
}

export default Connexion;
