import React from "react";
import PropTypes from 'prop-types';

function Loading({message="Chargement..."}) {
    return <React.Fragment>
        
        <span>{message}</span>
    </React.Fragment>
}

Loading.propTypes = {
    message: PropTypes.string
}

export default Loading;