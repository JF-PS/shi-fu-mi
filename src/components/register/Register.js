import React, {useContext, useState} from 'react';
import {useHistory} from 'react-router-dom';
import UserContext from "../../contexts/UserContext.js";
import Loading from "../loading/Loading";
import './Register.css';

function Register() {
  const [mail, setMail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();
  const userContext = useContext(UserContext);

  return (
<section id="cover">
<div id="cover-caption">
    <div class="container">
    <div class="row text-white">
            <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10 mx-auto text-center form p-4">
                <h1 class="display-4 py-2 text-truncate">S'enregistrer</h1>
                <div class="px-2" className="login">
                <form onSubmit={(e) => {
            e.preventDefault();
            setIsLoading(true);
            userContext.register(mail, password, (u) => {alert("Bienvenue" + u.login); history.push("/");}, history.push("/"), () => setIsLoading(false));
        }}>
            <div class="form-group">
                <label for="login" class="sr-only">Login : </label>
                <input name="login" placeholder="mail/login" class="form-control" value={mail} onChange={(e) => setMail(e.target.value)}/>
            </div>
            <div class="form-group">
                <label for="password" class="sr-only">Password : </label>
                <input name="password" placeholder="pass" class="form-control" type="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
            </div>
            { isLoading ? <Loading/> : <button class="btn btn-primary btn-lg" type="submit">Valider</button> }
        </form>
                </div>
            </div>
        </div>
        </div>
</div>
</section>
  );
}

export default Register;

