import React,  { useEffect} from "react";


function Api() {



    useEffect(() => {
        function onKeyup(e) {
            if (e.key === 'v') {
                console.log("VALIDER")
            }
        }
        window.addEventListener('keyup', onKeyup);
        return () => window.removeEventListener('keyup', onKeyup);
    }, []);

    return (
        <div>
            <h1> Test </h1>
        </div>
    );
}

export default Api;
