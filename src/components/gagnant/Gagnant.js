import Webcam from "react-webcam";
import React, {useContext,useRef, useState, useEffect} from "react";
import PartieContext from "../../contexts/PartieContext.js";
import UserContext from "../../contexts/UserContext.js";
import partieService from "../../services/PartieService";
import {useHistory} from 'react-router-dom';
import './Gagnant.css';
import { NavLink } from 'react-router-dom';
import ChalengerContext from "../../contexts/ChalengerContext";
import scissors from "../../assets/images/scissors.png";
import rock from "../../assets/images/rock.png";
import paper from "../../assets/images/paper.png";
import logo from "../../assets/images/logo.png";
import {Hands} from "@mediapipe/hands";
import * as cam from "@mediapipe/camera_utils";
const Gagnant = () => {


    let passe = true;
    const webcamRef = useRef(null);
    const canvasRef = useRef(null);
    const connect = window.drawConnectors;
    let camera = null;

    const userContext = useContext(UserContext);
    const chalengerContext = useContext(ChalengerContext);
    const partieContext = useContext(PartieContext);
    const history = useHistory();
    const [img, setImg] = useState(logo);
    const [img2, setImg2] = useState(logo);
    let winner = "En attente de l'autre joueur ";

    let player1 = "";
    let player2 = "";

    if(partieContext.partie.signeUser1 != null && partieContext.partie.signeUser2 != null){
        winner = partieService.gameManagement(partieContext.partie.signeUser1, partieContext.partie.signeUser2);

        if(partieContext.partie.idUser1 === userContext.user._id) {
            player1 = userContext.user.login;
            player2 = chalengerContext.chalenger.login;
        }
        else{
            player1 = chalengerContext.chalenger.login;
            player2 = userContext.user.login;
        }

        if(winner === partieContext.partie.signeUser1) {
            winner = player1 + " avec : " + partieContext.partie.signeUser1;
        }
        else if (winner === partieContext.partie.signeUser2){
            winner = player2 + " avec : " + partieContext.partie.signeUser2;
        }
        else{
            winner = "Match Nul !";
        }

        if (partieContext.partie.signeUser1 === "rock" && img === logo){
            setImg(rock);
        }
        if (partieContext.partie.signeUser1 === "scissors" && img === logo){
            setImg(scissors);
        }
        if (partieContext.partie.signeUser1 === "paper" && img === logo){
            setImg(paper);
        }
    
    
        if (partieContext.partie.signeUser2 === "rock" && img2 === logo){
            setImg2(rock);
        }
        if (partieContext.partie.signeUser2 === "scissors" && img2 === logo){
            setImg2(scissors);
        }
        if (partieContext.partie.signeUser2 === "paper" && img2 === logo){
            setImg2(paper);
        }

    } 
    function wait(ms){
        var start = new Date().getTime();
        var end = start;
        while(end < start + ms) {
        end = new Date().getTime();
    }
    }

    const Repeat = () =>{
        wait(200);
       

       partieContext.getPartie(partieContext.partie._id, (p) => {
           if(p.signeUser2 === "" || p.signeUser1 === "") {
               console.log("Le chalenger réfléchis encore à son signe.");
           }
           else{
               history.push("/gagnant");
           }
       }, alert);


    }

    function onResults(results) {
        //const video = webcamRef.current.video;
        const videoWidth = webcamRef.current.video.videoWidth;
        const videoHeight = webcamRef.current.video.videoHeight;

        // Set canvas width
        canvasRef.current.width = videoWidth;
        canvasRef.current.height = videoHeight;

        const canvasElement = canvasRef.current;
        const canvasCtx = canvasElement.getContext("2d");

        canvasCtx.save();
        canvasCtx.clearRect(0, 0, canvasElement.width, canvasElement.height);
        canvasCtx.drawImage(
            results.image,
            0,
            0,
            canvasElement.width,
            canvasElement.height
        );

      
        Repeat();
 
        
        
        canvasCtx.restore();
        
    }

    useEffect(() => {


        const hands = new Hands({
            locateFile: (file) => {
                return `https://cdn.jsdelivr.net/npm/@mediapipe/hands/${file}`;
            }
        });

        hands.setOptions({
            maxNumHands: 1,
            minDetectionConfidence: 0.9,
            minTrackingConfidence: 0.9
        });
        
        hands.onResults(onResults);
        
        if (typeof webcamRef.current !== "undefined" && webcamRef.current !== null) {
            if (webcamRef.current.video !== null) {

                camera = new cam.Camera(webcamRef.current.video, {
                    onFrame: async () => {
                        await hands.send({image: webcamRef.current.video});
                    },
                    width: 640,
                    height: 480,
                });
                camera.start();
            }

        }

       
    }, []);

   



    const refresh = () => {

        console.log("Refresh")
        wait(3000);

        partieContext.getPartie(partieContext.partie._id, (p) => {
            if(p.signeUser2 === "" || p.signeUser1 === "") {
                alert("Le chalenger réfléchis encore à son signe.");
            }
            else{
                history.push("/gagnant");
            }
        }, alert);
    }

    const newGame = () => {
        partieContext.reloadPartie(partieContext.partie._id, (p) => {
            console.log(p);
            history.push("/start");
        });
    }

    return(

        <div className="App">
            <center>
            <div className="App">
           
            
                    <Webcam
                        ref={webcamRef}
                        style={{
                            visibility: "hidden",
                            position: "absolute",
                            marginLeft: "auto",
                            marginRight: "auto",
                            left: 0,
                            right: 0,
                            textAlign: "center",
                            zindex: 9,
                            width: 640,
                            height: 480,
                        }}
                    /></div>
                    {" "}
                    <canvas
                        ref={canvasRef}
                        className="output_canvas"
                        style={{
                            visibility: "hidden",
                            position: "absolute",
                            marginLeft: "auto",
                            marginRight: "auto",
                            left: 0,
                            right: 0,
                            textAlign: "center",
                            zindex: 9,
                            width: 640,
                            height: 480,
                        }}
                    />
                
            </center>

            
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-11"></div>
                            <div class="col-md-1">

                                <button id="actualiser" class="btn btn-outline-secondary"  onClick={refresh} >
                                    Actualiser
                                </button>

                            </div>
                        </div>

                     {/* <div class="row"> 
                   
                         <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-4"><img id="image" src={logo} alt="camera"/></div>
                            <div class="col-md-2"></div>
                            <div class="col-md-4"><img id="image" src={logo} alt="camera"/></div>
                            <div class="col-md-1"></div>
                
                        </div> */}

                        <div class="row">
                            <div class="col-md-2"></div>

            
  
                            <div class="col-md-3">
                                <h2>{player1}</h2>
                                <img id="image2" src={img} alt="signe" style={{ width: 440, height: 360 }}/>
                            </div>

                            <div class="col-md-2"><img id="image3" src="https://st3.depositphotos.com/1800192/14757/v/600/depositphotos_147572465-stock-illustration-versus-sign-in-comic-style.jpg" alt="Canvas Logo"/></div>

                            <div class="col-md-3">
                                <h2>{player2}</h2>
                                <img id="image2" src={img2} alt="signe" style={{ width: 440, height: 360 }}/>
                            </div>



                            <div class="col-md-2"></div>
                

                        <div class="row">
                        
                            <div class="col-md-3"></div>
                           
                            <div class="col-md-6">
                                <ul class="list-group">
                                    <li id="textGagnant" class="list-group-item">{winner}</li>
                                </ul>

                                <button id="butRejouer" onClick={ () => newGame()  } class="btn btn-secondary">
                                    Rejouer
                                </button>

                            </div>
                            
                            <div class="col-md-3"></div>
                
                        </div>
                        {/* </div> */}
                       
                </div>

                </div>
            </div>
        </div>

    );
}
  
export default Gagnant;